/**
 * Skill Test For WordPress
 *
 * Copyright (c) 2019 Pritom Chowdhury Dip
 * Licensed under the GPLv2+ license.
 */

/*jslint browser: true */
/*global jQuery:false */
/*global stw:false */

window.Project = (function (window, document, $, undefined) {
	'use strict';
	var app = {
        init: function () {
			$('.skill-test1-submit').on('click', app.submitPostOnClick );
		},
		submitPostOnClick:function(e){

			e.preventDefault();
			
			var title = $('#post_title').val();
			var desc  = $('#post_desc').val();
			var cat   = $('#cat').val();
			var tag   = $('#post_tag').val();

			var data  = {
				'action': 'skill_test_for_wp',
				'nonce' : stw.nonce,
				'title' : title,
				'desc'	: desc,
				'cat'	: cat,
				'tag' 	: tag
			};

			wp.ajax.send('skill_test_for_wp', {
				data: data,
				dataType : 'json',
				success: function (res) {
					window.location = res.url;
					console.log('success = ' + res);
				},
				error: function (error) {
					console.log(error);
					$('.skill-test-error-message').append('<p>' + error + '</p>');
				}
			});

		}
    };
	$(document).ready(app.init);

	return app;

})(window, document, jQuery);
