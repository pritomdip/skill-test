/**
 * Skill Test For WordPress
 * 
 *
 * Copyright (c) 2019 Pritom Chowdhury Dip
 * Licensed under the GPLv2+ license.
 */

/*jslint browser: true */
/*global jQuery:false */

window.Project = (function (window, document, $, undefined) {
    'use strict';

    var app = {
        initialize: function () {          

        }
        
    };
 
    $(document).ready(app.initialize);
    
    return app; 
})(window, document, jQuery);