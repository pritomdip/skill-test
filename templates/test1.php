<div class="skill-test1-wrapper">
	
	<form class="skill-test-form" method="POST" action="">

		<h3 class="skill-test1-logo"><?php _e('Adding a new post', 'skill-test-for-wp'); ?></h3>

		<div class="skill-test-form-group">
			<label for="post_title"><?php _e('Post Title', 'skill-test-for-wp'); ?></label>
			<input class="skill-test1-input" type="text" id="post_title" name="post_title" required />
		</div>

		<div class="skill-test-form-group">
			<label for="post_desc"><?php _e('Post Description', 'skill-test-for-wp'); ?></label>
			<textarea class="skill-test1-input" id="post_desc" name="post_desc"></textarea>
		</div>

		<div class="skill-test-form-group">
			<label for="cat"><?php _e('Category', 'skill-test-for-wp'); ?></label>
			<?php 
				wp_dropdown_categories( 'show_option_none=Category&tab_index=6&taxonomy=category&multiple=true&hide_empty=0&selected=0&exclude=1' ); 
			?>
		</div>

		<div class="skill-test-form-group">
			<label for="post_tag"><?php _e('Tags', 'skill-test-for-wp'); ?></label>
			<input class="skill-test1-input" type="text" value="" tabindex="5" size="16" name="post_tags" id="post_tag" />
		</div>

		<p class="skill-test-error-message"></p>

		<div class="skill-test-form-group btn-wrapper">
			<input style="background: <?php echo $btn_bg_clr; ?>; color: <?php echo $btn_text_clr; ?>;" class="skill-test1-submit" type="submit" name="submit" value="<?php echo $btn_text; ?>" />
		</div>

		<input type="hidden" name="action" value="skill_test_for_wp" />
		<?php wp_nonce_field( 'skill_test_for_wp' ); ?>

	</form>

</div>