<?php

namespace pritom\SkillTestForWp;

class Skill_Test_Ajax_Form {
    /**
     * API constructor.
    */
    public function __construct() {
        add_action('wp_ajax_skill_test_for_wp', array( $this, 'save_skill_test_post_data'));
		add_action('wp_ajax_nopriv_skill_test_for_wp', array( $this, 'save_skill_test_post_data') );
    }

    /**
     * Save Post Data coming from AJAX Request
     *
     * @since 1.0.0
    */

    public function save_skill_test_post_data(){

        if ( !is_user_logged_in() ) {
           wp_send_json_error( 'User must be logged in to submit posts.' );
        }

        if (! isset( $_POST['nonce'] ) || !wp_verify_nonce($_POST['nonce'],'skill_test_for_wp')){
            wp_send_json_error( 'No scope for cheating with the security' );
        }

        $title = sanitize_text_field($_POST['title']);
        $desc  = wp_kses_post($_POST['desc']);
        $cat   = intval($_POST['cat']);
        $tag   = sanitize_text_field($_POST['tag']);

        if ( !isset ($title) || empty( $title ) ) {
            wp_send_json_error( 'Post title is required. Please give a title to that post.' );
        }

        $created_post = array(
            'post_title'    => $title,
            'post_content'  => $desc,
            'post_type'     => 'post',
            'post_status'   => 'publish',
            'tags_input'    => array($tag)  
        );
        
        $post_id = wp_insert_post($created_post); 

        wp_set_post_terms( $post_id,(array)($_POST['cat']),'category', true );

        if( $post_id ){

            $url = get_post_permalink( $post_id );

            wp_send_json_success([
                'url' => $url,
            ]);

        } else {
            wp_send_json_error( 'Something went wrong. Please try again later.' );
        }
    }
}