<?php

namespace Pritom\SkillTestForWp\Admin;

use Pritom\SkillTestForWp\Admin\Settings_API;

class Settings {
	private $settings_api;

	function __construct() {
		$this->settings_api = new Settings_API();
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_menu', array($this, 'admin_menu'));
	}

	function admin_init() {
		//set the settings
		$this->settings_api->set_sections($this->get_settings_sections());
		$this->settings_api->set_fields($this->get_settings_fields());

		//initialize settings
		$this->settings_api->admin_init();
	}

	function get_settings_sections() {
		$sections = array(
			array(
				'id'    => 'stw_settings',
				'title' => __( 'Skill Test Settings', 'skill-test-for-wp' )
			),
		);

		return apply_filters( 'stw_settings_sections', $sections );
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	function get_settings_fields() {

		$settings_fields = array(

			'stw_settings' => array(

				array(
					'name' 	  => 'test1_btn_text',
					'label'   => __('Skill Test1 Button Text', 'skill-test-for-wp'),
					'type'    => 'text',
					'default' => 'Submit',
				),

				array(
					'name' 	  => 'test1_btn_bg_color',
					'label'   => __('Skill Test1 Button Background Color', 'skill-test-for-wp'),
					'type'    => 'color',
					'default' => '#e67e22',
				),
				array(
					'name' 	  => 'test1_btn_font_color',
					'label'   => __('Skill Test1 Button Font Color', 'skill-test-for-wp'),
					'type'    => 'color',
					'default' => '#fff',
				),
			),
		);

		return apply_filters('stw_settings_fields', $settings_fields);
	}

	/**
	 * Add Skill Test Gallary settings sub menu to Skill Test admin menu
	 *
	 * @since 1.0.0
	 */

	function admin_menu() {


		add_menu_page( 
	        __( 'Skill Test Settings', 'skill-test-for-wp' ),
	        'Skill Test Settings',
	        'manage_options',
	        'stw_settings',
	        array( $this, 'settings_page' ),
	        'dashicons-admin-tools',
	        6
	    ); 

	}

	/**
	 * Menu page for Skill Test Gallery sub menu
	 *
	 * @since 1.0.0
	 */

	function settings_page() {
		echo '<div class="wrap">';
		echo sprintf("<h2>%s</h2>", __('Skill Test Settings', 'skill-test-for-wp'));
		echo '<h3>Use this shortcode to display user form-> [skill_test1] </h3>';
		$this->settings_api->show_settings();
		echo '</div>';
	}
}




