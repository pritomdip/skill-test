<?php

namespace pritom\SkillTestForWp;

class Frontend {
	/**
	 * The single instance of the class.
	 *
	 * @var Frontend
	 * @since 1.0.0
	 */
	protected static $init = null;

	/**
	 * Frontend Instance.
	 *
	 * @since 1.0.0
	 * @static
	 * @return Frontend - Main instance.
	 */
	public static function init() {
		if ( is_null( self::$init ) ) {
			self::$init = new self();
			self::$init->setup();
		}

		return self::$init;
	}

	/**
	 * Initialize all frontend related stuff
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function setup() {
		$this->includes();
		$this->init_hooks();
		$this->instance();
	}

	/**
	 * Includes all frontend related files
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function includes() {
		require_once dirname( __FILE__ ) . '/class-shortcodes.php';
		require_once dirname( __FILE__ ) . '/class-form-ajax.php';
		require_once dirname( __FILE__ ) . '/class-exclude-users.php';

	}

	/**
	 * Register all frontend related hooks
	 *
	 * @since 1.0.0
	 * @return void
	 */
	private function init_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Fire off all the instances
	 *
	 * @since 1.0.0
	 */
	protected function instance() {
		new Shortcode();
		new Skill_Test_Ajax_Form();
		new Exclude_Users();
	}

	/**
	 * Loads all frontend scripts/styles
	 *
	 * @param $hook
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function enqueue_scripts( $hook ) {
		wp_register_style('skill-test-for-wp', STW_ASSETS_URL."/css/frontend.css", STW_VERSION);
		wp_register_script('skill-test-for-wp', STW_ASSETS_URL."/js/frontend/frontend.js", ['jquery', 'wp-util'], STW_VERSION, true);

		wp_localize_script('skill-test-for-wp', 'stw',
		[
			'ajaxurl'   => admin_url('admin-ajax.php'),
			'nonce'     => wp_create_nonce('skill_test_for_wp')
		]);		
		
		wp_enqueue_style('skill-test-for-wp');
		wp_enqueue_script('skill-test-for-wp');
	}

}

Frontend::init();
