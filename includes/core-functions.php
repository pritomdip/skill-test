<?php
//function prefix stw

//Filtering wp_dropdown Categories to multiple select

function stw_multiple_categories_dropdown( $output, $r ) {

    if( isset( $r['multiple'] ) && $r['multiple'] ) {

         $output = preg_replace( '/^<select/i', '<select multiple', $output );

        $output = str_replace( "name='{$r['name']}'", "name='{$r['name']}[]'", $output );

        foreach ( array_map( 'trim', explode( ",", $r['selected'] ) ) as $value )
            $output = str_replace( "value=\"{$value}\"", "value=\"{$value}\" selected", $output );

    }

    return $output;
}

add_filter( 'wp_dropdown_cats', 'stw_multiple_categories_dropdown', 10, 2 );

// Skill test 3 & 4 getting every theme version

function stw_get_theme_version(){

	$theme_data = wp_get_theme();
	$version    = $theme_data->get( 'Version' );
	?>

	<div class="skill-test3-footer-wrapper">
		<div class="skill-test-left-version skill-test-footer">
    		<span><?php echo _e('Test 3 (Theme Version)  : ', 'skill-test-for-wp'); ?></span>
    		<span><?php echo $version; ?></span>
    	</div>
    	<div class="skill-test-right-version skill-test-footer">
    		<span><?php echo _e('Test 4 (Theme Version)  : ', 'skill-test-for-wp'); ?></span>
    		<span><?php echo $version; ?></span>
    	</div>
    </div>

	<?php
} 

add_action('wp_footer', 'stw_get_theme_version', 100);