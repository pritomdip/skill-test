<?php

namespace pritom\SkillTestForWp;

class Shortcode {
    /**
     * Shortcode constructor.
    */
    public function __construct() {
        add_shortcode('skill_test1', array($this, 'render_skill_test1'));
    }

    /**
     * Creating Shortcode for viewing Post Form to users
     *
     * @since 1.0.0
    */

    public function render_skill_test1($attr){

    	$params       = shortcode_atts( [ 'id' => null, 'option' => '' ], $attr );

        $options      = get_option('stw_settings');

        $btn_text     = empty($options['test1_btn_text']) ? 'Submit' : esc_attr($options['test1_btn_text']);

        $btn_bg_clr   = empty($options['test1_btn_bg_color']) ? '#e67e22' : esc_attr($options['test1_btn_bg_color']);

        $btn_text_clr = empty($options['test1_btn_font_color']) ? '#fff' : esc_attr($options['test1_btn_font_color']);

    	ob_start();	

		include STW_TEMPLATES_DIR . '/test1.php';

		do_action( 'skill_test1_after_html_content');

        $html = ob_get_clean();
		    
		return $html;

    }
}