<?php
namespace pritom\SkillTestForWp;

class Exclude_Users{
	/**
     * Exclude Users constructor.
    */
    public function __construct() {
        add_filter( 'bp_after_has_members_parse_args', array( $this, 'exclude_admin_users_by_role' ));
    	add_filter('bp_get_total_member_count', array( $this, 'count_total_after_excluding' ));
    }

    /**
	 * Exclude Admin Users.
	 *
	 * @since 1.0.0
	*/

    public function exclude_admin_users_by_role( $args ) {
    
	    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
	        return $args;
	    }
	 
	    $excluded = isset( $args['exclude'] ) ? $args['exclude'] : array();
	 
	    if ( ! is_array( $excluded ) ) {
	        $excluded = explode( ',', $excluded );
	    }

	    $role     = ['administrator'];
	    $user_ids = get_users( array( 'role__in' => $role, 'fields' => 'ID' ) );
	 
	    $excluded = array_merge( $excluded, $user_ids );
	 
	    $args['exclude'] = $excluded;
	 
	    return $args;
	}

	/**
	 * Count total after excluding admin.
	 *
	 * @since 1.0.0
	*/

    public function count_total_after_excluding($count){

    	$role     = ['administrator'];
	    $users 	  = get_users( array( 'role__in' => $role, 'fields' => 'ID' ) );

	    $number_of_users = count($users);

	    return $count - $number_of_users;
	}

}